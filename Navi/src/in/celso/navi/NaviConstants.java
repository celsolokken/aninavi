package in.celso.navi;

import java.awt.Color;

public class NaviConstants {
	
	public static final int SERIES = 1, ANIME = 2, MANGA = 3, VN = 4, IMGS = 5;
	
	public static final Color[] COLORS = new Color[] {Color.WHITE, new Color(115, 80, 90), new Color(34, 76, 183),
		new Color(50, 50, 50), new Color(30, 90, 16), new Color(190, 190, 230)};

}
