package in.celso.navi.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;

public class RankPicker extends JComponent {

	private int starWidth;
	private int strWidth;
	private int max;
	private int pseudoValue;
	private int value;
	
	private boolean heightKnown;
	
	public RankPicker(int max) {
		this.max = max;
		addMouseListener(mouseAdapter());
	}
	
	public RankPicker(int max, Color c1, Color c2) {
		this(max);
		setBackground(c1);
		setForeground(c2);
	}

	protected void paintComponent(Graphics g0) {
		Graphics g = (Graphics2D) g0;

		if (strWidth == 0) {
			strWidth = g.getFontMetrics().stringWidth("★★★★★");
		}

		if (starWidth == 0) {
			starWidth = g.getFontMetrics().stringWidth("★");
		}
		
		String str = "", str2 = "";
		for (int i = 1; i <= max; i++) {
			if (i <= value)
				str += "★";
			else
				str2 += "★";
		}
		
		g.setColor(getForeground());
		g.drawString(str, getWidth() / 2 - strWidth / 2, 10);
		
		g.setColor(getBackground());
		g.drawString(str2, getWidth() / 2 - strWidth / 2 + (value * starWidth), 10);
	}

	public Dimension getPreferredSize() {
		return new Dimension(140, getFont().getSize());
	}

	private MouseAdapter mouseAdapter() {
		return new MouseAdapter() {
			public void mouseMoved(MouseEvent e) {
				int v = getValueAt(e.getX());

				if (pseudoValue != v) {
					pseudoValue = v;
					repaint();
				}
			}

			public void mouseExited(MouseEvent e) {
				if (value != pseudoValue) {
					repaint();
				}
			}

			public void mousePressed(MouseEvent e) {
				int v = getValueAt(e.getX());

				if (value != v) {
					value = v;
					repaint();
				}
			}
		};
	}

	private int getValueAt(float mouseX) {
		float sx = getWidth() / 2f - strWidth / 2f;
		
		if (mouseX < sx)
			return 0;
		
		float result = (mouseX - sx) / (starWidth);
		
		return (int) result + 1;
	}

}
