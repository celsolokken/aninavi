package in.celso.navi.gui;

import in.celso.navi.NaviConstants;
import in.celso.navi.NaviItemListener;
import in.celso.navi.Series;
import in.celso.navi.anime.Anime;
import in.celso.navi.manga.Manga;
import in.celso.navi.visualnovel.VisualNovel;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class SeriesPanel extends OPanel implements NaviItemListener {
	
	private Navi navi;
	private Series series;
	
	private static final JPopupMenu popupMenu = new JPopupMenu();
	static {
		JMenuItem addAnime = new JMenuItem("Create anime...");
		addAnime.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SeriesPanel sp = (SeriesPanel) ((JPopupMenu) ((Component) e.getSource()).getParent()).getInvoker();
				sp.navi.setCurrentPanel(new CreatorDialog(sp.navi, NaviConstants.ANIME, sp.series), true);
			}
		});
		popupMenu.add(addAnime);
		
		JMenuItem addManga = new JMenuItem("Create manga...");
		addManga.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SeriesPanel sp = (SeriesPanel) ((JPopupMenu) ((Component) e.getSource()).getParent()).getInvoker();
				sp.navi.setCurrentPanel(new CreatorDialog(sp.navi, NaviConstants.MANGA, sp.series), true);
			}
		});
		popupMenu.add(addManga);
		
		JMenuItem addVn = new JMenuItem("Create visual novel...");
		addVn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SeriesPanel sp = (SeriesPanel) ((JPopupMenu) ((Component) e.getSource()).getParent()).getInvoker();
				sp.navi.setCurrentPanel(new CreatorDialog(sp.navi, NaviConstants.VN, sp.series), true);
			}
		});
		popupMenu.add(addVn);
	}
	
	protected SeriesPanel(Navi navi, Series series) {
		this.navi = navi;
		this.series = series;
		setLayout(new VerticalFlowLayout(FlowLayout.CENTER, 20, 20));
		
		setComponentPopupMenu(popupMenu);
		
		for (Anime anime : series.animeList) 
			add(new NaviItem(anime).setOwner(this));
		
		for (Manga manga : series.mangaList)
			add(new NaviItem(manga).setOwner(this));
		
		for (VisualNovel vn : series.vnList)
			add(new NaviItem(vn).setOwner(this));
	}

	public void onClick(NaviItem item) {
		navi.naviItemClicked(item);
	}

}
