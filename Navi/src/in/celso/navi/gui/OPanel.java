package in.celso.navi.gui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class OPanel extends JPanel {
	
	public void paintComponent(Graphics g) {
		int wid = getWidth();
		int hei = getHeight();
		int bgSize = 64;
		for (int x = 0; x <= wid / bgSize; x++)
			for (int y = 0; y <= hei / bgSize; y++) {
				int g2 = 244 - ((x%2)+10+y)*4, b2 = 182 - ((x%2)+10+y)*4;
				g.setColor(new Color(255, g2 > 0 ? g2 : 0, b2 > 0 ? b2 : 0, 255));
				g.fillRect(x * bgSize, y * bgSize, bgSize, bgSize);
			}
	}

}
