package in.celso.navi.gui;

import in.celso.navi.Entry;
import in.celso.navi.ImageCache;
import in.celso.navi.NaviConstants;
import in.celso.navi.NaviItemListener;
import in.celso.navi.Series;
import in.celso.navi.Utils;
import in.celso.navi.anime.Anime;
import in.celso.navi.manga.Manga;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import org.jdesktop.swingx.border.DropShadowBorder;

public class NaviItem extends JComponent implements Comparable {
	
	private static final int COVER_WID = 120, COVER_HEI = 160;
	
	private static final int BORDER_SIZE = 6;
	
	private static final Dimension DIMENSION = new Dimension(COVER_WID + 6, COVER_HEI + 16 + 6 + 50);
	
	private static Image ylevelImg, oneshotImg, nsfwImg, incompleteImg;
	static {
		try {
			ylevelImg = Utils.createImage("~circle.png");
			oneshotImg = Utils.createImage("~oneshot.png");
			nsfwImg = Utils.createImage("~radio.png");
			incompleteImg = Utils.createImage("~circle.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static final MouseAdapter LISTENER = new MouseAdapter() {
		public void mouseExited(MouseEvent e) {
			//((JFrame) ((NaviItem) e.getSource()).getParent().getParent().getParent()
					//.getParent().getParent()).setCursor(Cursor.getDefaultCursor());
		}
		
		public void mouseEntered(MouseEvent e) {
			//((JFrame) ((NaviItem) e.getSource()).getParent().getParent().getParent()
					//.getParent().getParent()).setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));	
		}
		
		public void mouseClicked(MouseEvent e) {
			if (SwingUtilities.isLeftMouseButton(e)) {
				NaviItem ni = ((NaviItem) e.getSource());
				ni.owner.onClick(ni);
			}
		};
	};
	
	private NaviItemListener owner;
	
	private Image cover;
	
	protected int type;
	
	protected Series series;
	
	protected Entry entry;
	
	public NaviItem(Series series) {
		type = NaviConstants.SERIES;
		this.series = series;
		cover = ImageCache.getImage(series);
		addMouseListener(LISTENER);
		
		DropShadowBorder dsb = new DropShadowBorder(Color.LIGHT_GRAY, BORDER_SIZE);
		dsb.setShadowOpacity(0.3f);
		dsb.setShowBottomShadow(true);
		dsb.setShowTopShadow(true);
		dsb.setShowLeftShadow(true);
		dsb.setShowRightShadow(true);
		setBorder(dsb);
	}
	
	public NaviItem(Entry entry) {
		type = entry instanceof Anime ? NaviConstants.ANIME : entry instanceof Manga ? NaviConstants.MANGA : NaviConstants.VN;
		this.entry = entry;
		cover = ImageCache.getImage(entry);
		addMouseListener(LISTENER);
		
		DropShadowBorder dsb = new DropShadowBorder(Color.LIGHT_GRAY, BORDER_SIZE);
		dsb.setShadowOpacity(0.3f);
		dsb.setShowBottomShadow(true);
		dsb.setShowTopShadow(true);
		dsb.setShowLeftShadow(true);
		dsb.setShowRightShadow(true);
		setBorder(dsb);
	}
	
	public NaviItem setOwner(NaviItemListener owner) {
		this.owner = owner;
		return this;
	}
	
	public Dimension getPreferredSize() {
		return new Dimension(DIMENSION.width + BORDER_SIZE*2, DIMENSION.height + BORDER_SIZE*2);
	}
	
	public void paint(Graphics g0) {
		Graphics2D g = (Graphics2D) g0;
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		paintBorder(g);
		g.clipRect(BORDER_SIZE, BORDER_SIZE, getWidth() - BORDER_SIZE*2, getHeight() - BORDER_SIZE*2);
		g.translate(BORDER_SIZE, BORDER_SIZE);
		
		g.setColor(NaviConstants.COLORS[type]);
		g.fillRect(0, 0, DIMENSION.width, DIMENSION.height);
		if (cover != null)
			g.drawImage(cover, 3, 16 + 3, COVER_WID, COVER_HEI, null);
		else {
			g.setColor(Color.BLACK);
			g.fillRect(3, 16 + 3, COVER_WID, COVER_HEI);
		}
		
		g.setColor(new Color(0, 0, 0, 128));
		g.fillRect(3, 16 + 3 + COVER_HEI + 2, COVER_WID, 48);
		
		FontMetrics fm = g.getFontMetrics();
		
		g.setColor(Color.WHITE);
		String name = type == 1 ? series.name : entry.name;
		Rectangle2D nameRect = fm.getStringBounds(name, g);
		if (nameRect.getWidth() > COVER_WID - 6) {
			Vector<String> result = new Vector<>();
			String[] names = name.split(" ");
			double wided = 0;
			for (int i = 0, j = 0; i < names.length; i++) {
				Rectangle2D rect = fm.getStringBounds(names[i], g);
				wided += rect.getWidth();
				
				if (wided > COVER_WID - 6) {
					wided = rect.getWidth();
					j++;
				}
				
				if (result.size() <= j)
					result.add(names[i]);
				else {
					result.set(j, result.get(j) + " " + names[i]);
				}
			}
			
			float heied = 0;
			
			for (int i = 0, j = 0; i < result.size(); ) {
				if (j == 1) {
					result.remove(i);
					continue;
				}
				String str = result.get(i);
				Rectangle2D rect = fm.getStringBounds(str, g);
				heied += rect.getHeight();
				if (heied > 48) {
					heied -= rect.getHeight();
					heied += 3;
					result.set(i, "...");
					j = 1;
				}
				i++;
			}
			
			float y = 16 + 3 + COVER_HEI + 2 + 24 - (heied / 2);
			for (String str : result) {
				Rectangle2D rect = fm.getStringBounds(str, g);
				y += str.equals("...") ? 10 : rect.getHeight();
				g.drawString(str, DIMENSION.width / 2 - ((float) rect.getWidth() / 2), y - ((float) rect.getHeight() / 4));
			}
		}
		else {
			g.drawString(name, DIMENSION.width / 2 - (float) (nameRect.getWidth() / 2),
					16 + 3 + COVER_HEI + 2 + 24 + (float) (nameRect.getHeight() / 4));
		}
		
		if (type == NaviConstants.SERIES) {
			String animes = series.animeList.size() + "";
			String mangas = series.mangaList.size() + "";
			String vns = series.vnList.size() + "";
			
			g.setColor(NaviConstants.COLORS[NaviConstants.ANIME]);
			g.fillRect(3, 1, 20, 16);
			g.setColor(NaviConstants.COLORS[NaviConstants.MANGA]);
			g.fillRect(25, 1, 20, 16);
			g.setColor(NaviConstants.COLORS[NaviConstants.VN]);
			g.fillRect(48, 1, 20, 16);
			
			Rectangle2D aniRect = fm.getStringBounds(animes, g);
			Rectangle2D mangRect = fm.getStringBounds(mangas, g);
			Rectangle2D vnRect = fm.getStringBounds(vns, g);
			
			g.setColor(Color.WHITE);
			g.drawString(animes, 3 + 10 - (float) (aniRect.getWidth() / 2), 17 - (float) (aniRect.getHeight() / 4));
			g.drawString(mangas, 25 + 10 - (float) (mangRect.getWidth() / 2), 17 - (float) (mangRect.getHeight() / 4));
			g.drawString(vns, 48 + 10 - (float) (vnRect.getWidth() / 2), 17 - (float) (vnRect.getHeight() / 4));
		}
		else {
			int x = 3;
			
			/*g.drawImage(ylevelImg, x, 1, 16, 16, null);
			if (entry.isOneShot)
				g.drawImage(oneshotImg, x += 18, 1, 16, 16, null);
			if (entry.isNSFW)
				g.drawImage(nsfwImg, x += 18, 1, 16, 16, null);
			if (!entry.isComplete)
				g.drawImage(incompleteImg, x += 18, 1, 16, 16, null);
			
			g.setColor(Color.WHITE);
			
			String ylvl = entry.yLevel + "";
			Rectangle2D yrect = fm.getStringBounds(ylvl, g);
			g.drawString(ylvl, 3 + 8 - (float) (yrect.getWidth() / 2), 17 - (float) (yrect.getHeight() / 4));*/
			
			String ylvl = entry.yLevel == 0 ? "☆☆☆" : entry.yLevel == 1 ? "★☆☆" : entry.yLevel == 2 ? "★★☆" : "★★★";
			g.drawString(ylvl, 3 + COVER_WID - fm.stringWidth(ylvl), 14);
			
			if (!entry.isOfficial) {
				String d = "D";
				Rectangle2D drect = fm.getStringBounds(d, g);
				g.drawString(d, x + 18 + 4 - (float) (drect.getWidth() / 2), 17 - (float) (drect.getHeight() / 4));	
			}
		}
	}

	public int compareTo(Object o) {
		String s1 = type == NaviConstants.SERIES ? series.name : entry.name;
		String s2 = ((NaviItem) o).type == NaviConstants.SERIES ? ((NaviItem) o).series.name : ((NaviItem) o).entry.name;
		return s1.compareTo(s2);
	}

}
