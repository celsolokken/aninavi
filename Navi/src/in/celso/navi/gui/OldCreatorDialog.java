package in.celso.navi.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

import in.celso.navi.Entry;
import in.celso.navi.Series;

public class OldCreatorDialog {
	
	public static Series createSeries(JFrame owner) {
		final JDialog jd = new JDialog(owner, true);
		final JButton goButton = new JButton("Go");
		
		JTextField nameField = new JTextField();
		
		ActionListener go = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized (goButton) {
					goButton.notify();
				}
				
				jd.dispose();
			}
		};
		
		goButton.addActionListener(go);
		nameField.addActionListener(go);
		
		jd.setSize(350, 200);
		
		jd.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 1;
		
		jd.add(new JLabel("Name: "), c);
		
		c.gridx += c.gridwidth;
		c.gridwidth = 2;
		c.ipadx = 100;
		jd.add(nameField, c);
		
		c.gridx = 0;
		c.gridy++;
		c.gridwidth = 3;
		c.ipadx = 0;
		c.insets = new Insets(5, 50, 0, 50);
		jd.add(goButton, c);
		
		jd.setLocationRelativeTo(null);
		jd.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		jd.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				synchronized (goButton) {
					goButton.notify();
				}
				jd.dispose();
			}
		});
		
		jd.setVisible(true);
		
		synchronized (goButton) {
			try {
				goButton.wait();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
		
		String name = nameField.getText();
		if (name == null || name.length() == 0)
			return null;
		
		Series s = new Series();
		
		return s;
	}
	
	public static Entry createEntry(JFrame owner, Series series) {
		final JDialog jd = new JDialog(owner, true);
		final JButton goButton = new JButton("Go");
		
		JTextField nameField = new JTextField();
		JTextField authorField = new JTextField();
		JCheckBox oneshotBox = new JCheckBox("Is one shot?");
		JCheckBox doujinBox = new JCheckBox("Is doujinshi?");
		JCheckBox completeBox = new JCheckBox("Is complete?");
		JCheckBox nsfwBox = new JCheckBox("Is NSFW?");
		
		ActionListener go = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized (goButton) {
					goButton.notify();
				}
				
				jd.dispose();
			}
		};
		
		goButton.addActionListener(go);
		nameField.addActionListener(go);
		
		jd.setSize(350, 200);
		
		jd.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 1;
		
		jd.add(new JLabel("Name: "), c);
		
		c.gridx += c.gridwidth;
		c.gridwidth = 2;
		c.ipadx = 100;
		jd.add(nameField, c);
		
		c.gridx = 0;
		c.gridwidth = 1;
		c.ipadx = 0;
		c.gridy++;
		jd.add(new JLabel("Author: "), c);
		
		c.gridx += c.gridwidth;
		c.gridwidth = 2;
		c.ipadx = 100;
		jd.add(authorField, c);
		
		c.ipadx = 0;
		c.gridx = 0;
		c.gridy++;
		jd.add(oneshotBox, c);
		
		c.gridy++;
		jd.add(doujinBox, c);
		
		c.gridy++;
		jd.add(completeBox, c);
		
		c.gridy++;
		jd.add(nsfwBox, c);
		
		c.gridx = 0;
		c.gridy++;
		c.gridwidth = 3;
		c.ipadx = 0;
		c.insets = new Insets(5, 50, 0, 50);
		jd.add(goButton, c);
		
		jd.setLocationRelativeTo(null);
		jd.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		jd.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				synchronized (goButton) {
					goButton.notify();
				}
				jd.dispose();
			}
		});
		
		jd.setVisible(true);
		
		synchronized (goButton) {
			try {
				goButton.wait();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
		
		String name = nameField.getText();
		if (name == null || name.length() == 0)
			return null;
		
		return null;
	}

}
