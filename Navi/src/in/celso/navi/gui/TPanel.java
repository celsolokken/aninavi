package in.celso.navi.gui;

import java.awt.Color;

import javax.swing.JPanel;

public class TPanel extends JPanel {
	
	protected TPanel(Color color) {
		setBackground(color);
	}
	
	protected TPanel() {
		setBackground(new Color(0, 0, 0, 0));
	}

}
