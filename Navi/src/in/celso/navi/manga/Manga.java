package in.celso.navi.manga;

import java.util.List;

import in.celso.navi.Entry;

public class Manga extends Entry {
	
	public Manga(String name, String author, int chronoPosition,
			String prequel, String sequel, boolean isOneShot,
			boolean isOfficial, boolean isComplete,
			boolean isNSFW, int yLevel) {
		super(name, author, chronoPosition, prequel, sequel, isOneShot, isOfficial, isComplete, isNSFW, yLevel);
	}
	
	public Manga() {
		
	}
	
	public List<MangaChapter> chapters;
}
