package in.celso.navi.gui;

import in.celso.navi.Entry;
import in.celso.navi.Fetcher;
import in.celso.navi.NaviItemListener;
import in.celso.navi.NaviItemManager;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RoundRectangle2D;

import javax.swing.*;

import org.jdesktop.swingx.border.DropShadowBorder;

public class HomePanel extends OPanel implements NaviItemListener {
	
	private JPanel recentPanel;
	private Navi navi;

	protected HomePanel(final Navi navi) {
		this.navi = navi;
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new GridBagLayout());
		GridBagConstraints c0 = new GridBagConstraints();
		c0.fill = GridBagConstraints.BOTH;
		c0.anchor = GridBagConstraints.NORTH;
		c0.gridx = 0;
		c0.gridy = 0;
		c0.weightx = 1;
		c0.weighty = 1;
		c0.gridwidth = 1;
		c0.gridheight = 1;
		c0.insets = new Insets(0, 0, 0, 20);
		
		final int BORDER_SIZE = 6;
		
		JPanel searchSeries = new JPanel() {
			public void paintComponent(Graphics g) {
				paintBorder(g);
				g.clipRect(BORDER_SIZE, BORDER_SIZE, getWidth() - BORDER_SIZE*2, getHeight() - BORDER_SIZE*2);
				g.translate(BORDER_SIZE, BORDER_SIZE);
				GUtils.paintChess(g, getWidth(), getHeight(), 32, 135, 100, 110);
				g.translate(-BORDER_SIZE, -BORDER_SIZE);
			}
		};
		DropShadowBorder dsb = new DropShadowBorder(Color.LIGHT_GRAY, BORDER_SIZE);
		dsb.setShadowOpacity(0.3f);
		dsb.setShowBottomShadow(true);
		dsb.setShowTopShadow(true);
		dsb.setShowLeftShadow(true);
		dsb.setShowRightShadow(true);
		{
			searchSeries.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.HORIZONTAL;
			c.anchor = GridBagConstraints.NORTH;
			c.gridx = 0;
			c.gridy = 0;
			c.gridheight = 1;
			c.gridwidth = 3;
			
			c.gridx = 0;
			c.insets = new Insets(20, 0, 0, 0);
			searchSeries.add(new JLabel("Search series", JLabel.CENTER), c);
			
			c.insets = new Insets(0, 0, 0, 0);
			c.gridx = 0;
			c.gridy++;
			c.gridwidth = 1;
			searchSeries.add(new JLabel("Name: "), c);
			
			c.gridwidth = 2;
			c.gridx++;
			c.ipadx = 100;
			JTextField searchField;
			searchSeries.add(searchField = new JTextField(), c);
			
			c.gridy++;
			c.gridx = 0;
			c.gridwidth = 3;
			c.ipadx = 0;
			c.insets = new Insets(20, 0, 0, 0);
			searchSeries.add(new JLabel("Create Series", JLabel.CENTER), c);
			
			c.insets = new Insets(0, 0, 0, 0);
			c.gridy++;
			c.gridwidth = 1;
			searchSeries.add(new JLabel("Name: "), c);
			
			c.gridwidth = 2;
			c.gridx++;
			c.ipadx = 100;
			c.weighty = 1;
			JTextField createField = new JTextField();
			searchSeries.add(createField, c);
			
			searchField.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					NaviItemManager.NaviItemFilter filter = new NaviItemManager.NaviItemFilter();
					filter.series = true;
					filter.nameSearch = ((JTextField) e.getSource()).getText();
					SearchPanel sp = new SearchPanel(navi, filter);
					JScrollPane scroll = new JScrollPane(sp);
					scroll.getVerticalScrollBar().setUnitIncrement(5);
					scroll.getVerticalScrollBar().setUI(Navi.CUSTOM_SCROLLBAR);
					navi.setCurrentPanel(scroll, true);
					sp.doSearch();
				}
			});
			
			createField.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JTextField source = (JTextField) e.getSource();
					String str = source.getText();
					if (str.length() > 0)
						Fetcher.createSeries(str, str.toLowerCase().replaceAll(" ", ""));
					source.setText("");
				}
			});
			
			/*c.gridwidth = 3;
			c.gridx = 0;
			c.gridy++;
			c.weightx = 1;
			c.weighty = 1;
			c.fill = GridBagConstraints.VERTICAL;
			searchSeries.add(Box.createVerticalGlue(), c);*/
			
		}
		topPanel.add(searchSeries, c0);
		searchSeries.setPreferredSize(new Dimension(searchSeries.getPreferredSize().width + 20 + 20, searchSeries.getPreferredSize().height + 20 + 20));
		searchSeries.setBorder(dsb);
		for (Component c : searchSeries.getComponents())
			if (!(c instanceof JTextField) && !(c instanceof JButton))
				c.setForeground(Color.WHITE);
		
		c0.gridy = 0;
		c0.gridx++;
		c0.gridheight = 3;
		c0.insets = new Insets(0, 0, 0, 0);
		
		JPanel searchEntry = new JPanel() {
			public void paintComponent(Graphics g) {
				paintBorder(g);
				g.clipRect(BORDER_SIZE, BORDER_SIZE, getWidth() - BORDER_SIZE*2, getHeight() - BORDER_SIZE*2);
				g.translate(BORDER_SIZE, BORDER_SIZE);
				GUtils.paintChess(g, getWidth(), getHeight(), 32, 135, 100, 110);
				g.translate(-BORDER_SIZE, -BORDER_SIZE);
			}
		};
		{
			searchEntry.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 0;
			c.gridheight = 1;
			c.gridwidth = 8;
			Insets none = new Insets(0, 0, 0, 0);
			
			searchEntry.add(new JLabel("Search entry", JLabel.CENTER), c);
			
			c.gridy++;
			c.gridwidth = 2;
			searchEntry.add(new JLabel("Name: "), c);
			
			c.gridwidth = 4;
			c.gridx += 2;
			final JTextField nameField;
			searchEntry.add(nameField = new JTextField(), c);
			
			c.gridy++;
			c.gridwidth = 2;
			c.gridx = 0;
			searchEntry.add(new JLabel("Author: "), c);
			
			c.gridwidth = 4;
			c.gridx += 2;
			final JTextField authorField;
			searchEntry.add(authorField = new JTextField(), c);
			
			c.gridwidth = 2;
			c.gridy++;
			c.gridx = 0;
			final JCheckBox animeBox;
			searchEntry.add(animeBox = new JCheckBox("Anime", true), c);
			
			c.gridx += 2;
			c.insets = new Insets(0, 10, 0, 10);
			final JCheckBox mangaBox;
			searchEntry.add(mangaBox = new JCheckBox("Manga", true), c);
			
			c.gridx += 2;
			c.insets = none;
			final JCheckBox vnBox;
			searchEntry.add(vnBox = new JCheckBox("Visual Novel", true), c);
			
			final ButtonGroup oneshotGroup = new ButtonGroup();
			final JRadioButton oneshotYes, oneshotNo, oneshotIgnore;
			
			c.gridx = 0;
			c.gridy++;
			c.fill = GridBagConstraints.HORIZONTAL;
			searchEntry.add(new JLabel("One-shot: "), c);
			c.fill = GridBagConstraints.NONE;
			c.gridx += 2;
			searchEntry.add(oneshotYes = new JRadioButton("Yes"), c);
			c.gridx += 2;
			searchEntry.add(oneshotNo = new JRadioButton("No"), c);
			c.gridx += 2;
			searchEntry.add(oneshotIgnore = new JRadioButton("Ignore", true), c);
			
			oneshotGroup.add(oneshotYes);
			oneshotGroup.add(oneshotNo);
			oneshotGroup.add(oneshotIgnore);
			
			final ButtonGroup originalGroup = new ButtonGroup();
			final JRadioButton origYes, origNo, origIgnore;
			
			c.gridx = 0;
			c.gridy++;
			c.fill = GridBagConstraints.HORIZONTAL;
			searchEntry.add(new JLabel("Original: "), c);
			c.fill = GridBagConstraints.NONE;
			c.gridx += 2;
			searchEntry.add(origYes = new JRadioButton("Yes"), c);
			c.gridx += 2;
			searchEntry.add(origNo = new JRadioButton("No"), c);
			c.gridx += 2;
			searchEntry.add(origIgnore = new JRadioButton("Ignore", true), c);
			
			originalGroup.add(origYes);
			originalGroup.add(origNo);
			originalGroup.add(origIgnore);
			
			final ButtonGroup sfwGroup = new ButtonGroup();
			final JRadioButton sfwYes, sfwNo, sfwIgnore;
			
			c.gridx = 0;
			c.gridy++;
			c.fill = GridBagConstraints.HORIZONTAL;
			searchEntry.add(new JLabel("SFW: "), c);
			c.fill = GridBagConstraints.NONE;
			c.gridx += 2;
			searchEntry.add(sfwYes = new JRadioButton("Yes"), c);
			c.gridx += 2;
			searchEntry.add(sfwNo = new JRadioButton("No"), c);
			c.gridx += 2;
			searchEntry.add(sfwIgnore = new JRadioButton("Ignore", true), c);
			
			sfwGroup.add(sfwYes);
			sfwGroup.add(sfwNo);
			sfwGroup.add(sfwIgnore);
			
			c.gridwidth = 1;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy++;
			searchEntry.add(new JLabel("Min Y: "), c);

			c.fill = GridBagConstraints.NONE;
			c.gridx++;
			final JTextField minYField;
			searchEntry.add(minYField = new JTextField("0"), c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy++;
			searchEntry.add(new JLabel("Max Y:"), c);
			
			c.fill = GridBagConstraints.NONE;
			c.gridx++;
			final JTextField maxYField;
			searchEntry.add(maxYField = new JTextField("3"), c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridwidth = 3;
			c.gridx += 4;
			JButton search;
			searchEntry.add(search = new JButton("Search"), c);
			
			search.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					NaviItemManager.NaviItemFilter filter = new NaviItemManager.NaviItemFilter();
					filter.nameSearch = nameField.getText();
					filter.authorSearch = authorField.getText();
					filter.anime = animeBox.isSelected();
					filter.manga = mangaBox.isSelected();
					filter.vn = vnBox.isSelected();
					filter.oneshot = oneshotYes.isSelected() ? 1 : oneshotNo.isSelected() ? 2 : 0;
					filter.official = origYes.isSelected() ? 1 : origNo.isSelected() ? 2 : 0;
					filter.nsfw = sfwYes.isSelected() ? 2 : sfwNo.isSelected() ? 1 : 0;
					filter.minYlevel = Integer.parseInt(minYField.getText());
					filter.maxYlevel = Integer.parseInt(maxYField.getText());
					SearchPanel sp = new SearchPanel(navi, filter);
					JScrollPane scroll = new JScrollPane(sp);
					scroll.getVerticalScrollBar().setUnitIncrement(10);
					scroll.getVerticalScrollBar().setUI(Navi.CUSTOM_SCROLLBAR);
					navi.setCurrentPanel(scroll, true);
					sp.doSearch();
				}
			});
		}
		topPanel.add(searchEntry, c0);
		searchEntry.setPreferredSize(new Dimension(searchEntry.getPreferredSize().width + 40, searchEntry.getPreferredSize().height + 40));
		searchEntry.setBorder(dsb);
		for (Component c : searchEntry.getComponents())
			if (!(c instanceof JTextField) && !(c instanceof JButton))
				c.setForeground(Color.WHITE);
		
		setLayout(new BorderLayout());
		
		JPanel outerPane = new JPanel();
		outerPane.setLayout(new GridBagLayout());
		outerPane.add(topPanel);
		
		add(outerPane, BorderLayout.PAGE_START);
		topPanel.setMaximumSize(topPanel.getPreferredSize());
		
		//add(Box.createVerticalStrut(100));
		
		//add(new JSeparator());
		
		recentPanel = new OPanel();
		recentPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		Entry[] recents = Fetcher.getRecentEntries();
		for (Entry entry : recents)
			if (entry != null) {
				System.out.println("Recent: " + entry.name);
				recentPanel.add(new NaviItem(entry).setOwner(this));
			}

		JScrollPane recentScrollPane = new JScrollPane(recentPanel);
		recentScrollPane.getHorizontalScrollBar().setUnitIncrement(8);
		recentScrollPane.getHorizontalScrollBar().setUI(Navi.CUSTOM_SCROLLBAR);
		recentScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		
		add(recentScrollPane, BorderLayout.PAGE_END);
	}
	
	protected void addRecent(NaviItem entryItem) {
		recentPanel.remove(recentPanel.getComponentCount() - 1);
		recentPanel.add(entryItem, 0);
		recentPanel.getParent().revalidate();
	}

	public void onClick(NaviItem item) {
		navi.naviItemClicked(item);
	}
}

