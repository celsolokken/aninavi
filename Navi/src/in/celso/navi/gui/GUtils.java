package in.celso.navi.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;

public class GUtils {
	
	public static final Color NO_COLOR = new Color(0, 0, 0, 0);
	
	public static final int CENTER_ALIGNMENT = 1, RIGHT_ALIGNMENT = 2;
	
	public static void drawStringAligned(Graphics2D g, float posX, float posY, int alignment, String str) {
		Rectangle2D rect = g.getFontMetrics().getStringBounds(str, g);
		if (alignment == CENTER_ALIGNMENT) {
			g.drawString(str, posX - (float) rect.getWidth() / 2, posY + (float) rect.getHeight() / 3);
		}
		else {
			g.drawString(str, posX - (float) rect.getWidth(), posY + (float) rect.getHeight() / 3);
		}
	}
	
	public static void drawLinesAligned(Graphics2D g, float posX, float posY, int alignment, int lineHei, String... lines) {
		int wid = 0;
		for (int i = 0; i < lines.length; i++) {
			int w = g.getFontMetrics().stringWidth(lines[i]);
			if (i == 0 || w > wid)
				wid = w;
		}
		
		for (int i = 0; i < lines.length; i++) {
			g.drawString(lines[i], posX - (alignment == CENTER_ALIGNMENT ? wid/2f : wid), (posY + (i+1) * lineHei) + lineHei/3f);
		}
	}
	
	public static void paintBorderShadow(Graphics2D g2, int x, int y, int wid, int hei, int shadowWidth, int shadowOpacity) {
		paintBorderShadow(g2, x, y, wid, hei, shadowWidth, new Color(0, 0, 0, shadowOpacity));
	}
	
	public static void paintBorderShadow(Graphics2D g2, int x, int y, int wid, int hei, int shadowWidth, Color shadow) {
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		int sw = shadowWidth * 2;
		Rectangle rect = new Rectangle(wid, hei);
		for (int i = sw; i >= 2; i -= 2) {
			float pct = (float) (sw - i) / (sw - 1);
			g2.setColor(shadow);
			g2.setStroke(new BasicStroke(i));
			g2.translate(x, y);
			g2.draw(rect);
			g2.translate(-x, -y);
		}
	}
	
	public static void drawGradientRect(Graphics2D g, int x1, int y1, int x2, int y2, Color c1, Color c2) {
		g.setPaint(new GradientPaint(x1, y1, c1, x2, y2, c2));
		g.fillRect(x1, y1, x2 - x1, y2 - y1);
	}
	
	public static void paintChess(Graphics g, int wid, int hei, int quadSize, int red, int gre, int blu) {
		for (int x = 0; x <= wid / quadSize; x++)
			for (int y = 0; y <= hei / quadSize; y++) {
				int r2 = red - ((x%2)+10+y)*4, g2 = gre - ((x%2)+10+y)*4, b2 = blu - ((x%2)+10+y)*4;
				g.setColor(new Color(r2 > 0 ? r2 : 0, g2 > 0 ? g2 : 0, b2 > 0 ? b2 : 0, 255));
				g.fillRect(x * quadSize, y * quadSize, quadSize, quadSize);
			}
	}
	
	private static Color getMixedColor(Color c1, float pct1, Color c2, float pct2) {
	    float[] clr1 = c1.getComponents(null);
	    float[] clr2 = c2.getComponents(null);
	    for (int i = 0; i < clr1.length; i++) {
	        clr1[i] = (clr1[i] * pct1) + (clr2[i] * pct2);
	    }
	    return new Color(clr1[0], clr1[1], clr1[2], clr1[3]);
	}

}
