package in.celso.navi;

import in.celso.navi.anime.Anime;
import in.celso.navi.manga.Manga;
import in.celso.navi.visualnovel.VisualNovel;

import java.util.List;

public class Series {
	
	public String name;
	public String shortName;
	
	public List<Anime> animeList;
	public List<Manga> mangaList;
	public List<VisualNovel> vnList;

}
