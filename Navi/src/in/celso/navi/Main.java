package in.celso.navi;

import in.celso.navi.gui.Navi;

import java.awt.Color;
import java.awt.Insets;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class Main {

	public static void main(String[] args) {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		Color bg = new Color(246, 226, 221);

		UIManager.put("text", new Color(0, 0, 0));
		UIManager.put("RootPane.background", bg);
		UIManager.put("InternalFrame.background", bg);
		UIManager.put("Panel.background", new Color(0, 0, 0, 0));
		UIManager.put("TabbedPane.background", bg);
		UIManager.put("ScrollPane.background", new Color(0, 0, 0, 0));
		UIManager.put("ScrollPane.contentMargins", new Insets(0, 0, 0, 0));
		UIManager.put("TextField.background", bg);
		UIManager.put("nimbusFocus", new Color(255, 200, 150));
		UIManager.put("nimbusSelection", new Color(255, 200, 150));
		UIManager.put("textBackground", new Color(255, 200, 150));
		UIManager.put("textHighlight", new Color(255, 200, 150));
		UIManager.put("menuText", new Color(255, 255, 255));
		//UIManager.put("textHighlightText", new Color(0, 55, 255-150));

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Navi().setVisible(true);
			}
		});
	}

}
