package in.celso.navi.gui;

import in.celso.navi.Fetcher;
import in.celso.navi.NaviItemListener;
import in.celso.navi.NaviItemManager;
import in.celso.navi.NaviItemManager.NaviItemFilter;
import in.celso.navi.Series;

import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.SwingUtilities;

public class SearchPanel extends OPanel implements NaviItemListener {
	
	private Navi navi;
	
	private NaviItemManager nim;
	private List<NaviItem> items;
	
	protected SearchPanel(Navi navi, NaviItemFilter filter) {
		this.navi = navi;
		nim = new NaviItemManager(filter, items = new ArrayList<>());
		setup();
	}
	
	private void setup() {
		setLayout(new VerticalFlowLayout(FlowLayout.CENTER, 20, 20));
	}
	
	public void addEntriesForSeries(Series series) {
		
	}
	
	public void doSearch() {
		final SearchPanel instance = this;
		
		if (true) {
			items = nim.getList();
			Collections.sort(items);
			for (NaviItem ni : items) {
				add(ni.setOwner(this));
				//Fetcher.addRecent(ni.entry);
			}
			//Fetcher.saveRecents();
			revalidate();
			getParent().repaint();
			return;
		}
		
		nim.start();
		int added = 0;
		
		while (true) {
			synchronized (items) {
				//System.out.println("Waiting for items at Thread " + Thread.currentThread().getName());
				try {
					items.wait(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				//System.out.println("Finished waiting " + items.size());
				
				if (items.size() == ++added) {
					final int next = added - 1;
					
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							NaviItem ni = items.get(next);
							add(ni.setOwner(instance));
							revalidate();
							getParent().repaint();
						}
					});
				}
				else
					break;
			}
		}
	}

	public void onClick(NaviItem ni) {
		navi.naviItemClicked(ni);
	}
	
}
