package in.celso.navi.gui;

import in.celso.navi.Entry;
import in.celso.navi.Fetcher;
import in.celso.navi.ImageCache;
import in.celso.navi.anime.Anime;
import in.celso.navi.manga.Manga;
import in.celso.navi.visualnovel.VisualNovel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.jdesktop.swingx.border.DropShadowBorder;

public class EntryPanel extends OPanel {
	
	private JPanel topPanel;
	private JPanel bottomPanel;
	
	private static final Font SMALL_FONT = new Font("Dialog", Font.PLAIN, 10);
	private static final Font MEDIUM_FONT = new Font("Dialog", Font.PLAIN, 14);
	private static final Font TITLE_FONT = new Font("Dialog", Font.BOLD, 16);
	
	private static MouseAdapter mouseAdapter = getMouseAdapter();
	
	protected EntryPanel(Entry entry) {
		JSplitPane mainSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topPanel = createTop(entry), bottomPanel = createBottom());
		mainSplit.setEnabled(false);
		mainSplit.setDividerSize(0);
		setLayout(new FlowLayout());
		add(mainSplit);
	}

	private JPanel createTop(final Entry entry) {
		final Image cover = ImageCache.getImage(entry);
		
		final Image seriesImg = ImageCache.getImage("~list24.png");
		final Image editImg = ImageCache.getImage("~edit24.png");
		final Image addImg = ImageCache.getImage("~add24.png");
		final Image deleteImg = ImageCache.getImage("~trash24.png");
		
		JPanel tp = new JPanel() {
			public void paintComponent(Graphics g) {
				int wid = getWidth(), hei = getHeight();
				
				((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
				
				if (entry instanceof Manga)
					GUtils.paintChess(g, wid, hei, 32, 110, 110, 110);
				else if (entry instanceof Anime)
					GUtils.paintChess(g, wid, hei, 32, 110, 110, 180);
				else if (entry instanceof VisualNovel)
					GUtils.paintChess(g, wid, hei, 32, 100, 150, 100);
				
				//GUtils.paintBorderShadow((Graphics2D) g, 10, 10, 120, 160, 10, 15);
				g.drawImage(cover, 10, 10, null);
				
				g.clipRect(20 + 120, 10, wid - 30 - 120, hei - 20);
				g.translate(20 + 120, 10);
				Rectangle rect = g.getClip().getBounds();
				int w = rect.width, h = rect.height;
				//g.setColor(Color.BLACK);
				//g.fillRect(0, 0, w, h);
				
				g.setColor(Color.WHITE);
				
				g.setFont(TITLE_FONT);
				((Graphics2D) g).drawString(entry.name, 0, 16);
				
				//g.setFont(SMALL_FONT);
				//((Graphics2D) g).drawString("Y" + entry.yLevel , 2, h - 10);
				
				g.setFont(MEDIUM_FONT);
				int y = 44;
				((Graphics2D) g).drawString("Series: " + entry.series.name, 0, y);
				((Graphics2D) g).drawString("Author: " + entry.author, 0, y += 18);
				//if (entry.prequel != null)
					((Graphics2D) g).drawString("Prequel: " + entry.prequel, 0, y += 18);
				//if (entry.sequel != null)
					((Graphics2D) g).drawString("Sequel: " + entry.sequel, 0, y += 18);
					
				String ylvl = entry.yLevel == 0 ? "☆☆☆" : entry.yLevel == 1 ? "★☆☆" : entry.yLevel == 2 ? "★★☆" : "★★★";
				((Graphics2D) g).drawString("Y: " + ylvl, 0, y += 18);
				
				double hg = 10.5; //6.4
				//((Graphics2D) g).translate(0, hg);
				g.drawImage(seriesImg, w - 32 + 4, 4, 24, 24, null);
				
				((Graphics2D) g).translate(0, hg);
				g.drawImage(editImg, w - 32 + 4, 32 + 4, 24, 24, null);
				
				((Graphics2D) g).translate(0, hg);
				g.drawImage(addImg, w - 32 + 4, 64 + 4, 24, 24, null);
				
				((Graphics2D) g).translate(0, hg);
				g.drawImage(deleteImg, w - 32 + 4, 96 + 4, 24, 24, null);
			}
		};
		
		tp.setPreferredSize(new Dimension(600, 180));
		
		tp.setLayout(null);
		
		tp.addMouseListener(mouseAdapter);
		
		return tp;
	}
	
	private static MouseAdapter getMouseAdapter() {
		return new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int mx = e.getX(), my = e.getY();
				
				if (mx >= 450 + 20 + 120 - 32 + 4 && mx <= 450 + 20 + 120 - 32 + 4 + 24) {
					if (my >= 10 + 4 && my <= 10 + 4 + 24)
						System.out.println("1");
					else if (my >= 10 + 4 + 32 + 10.5 + 4 && my <= 10 + 4 + 32 + 4 + 10.5 + 24)
						System.out.println("2");
					else if (my >= 10 + 4 + 64 + 21 + 4 && my <= 10 + 4 + 64 + 4 + 21 + 24)
						System.out.println("3");
					else if (my >= 10 + 4 + 96 + 31.5 + 4 && my <= 10 + 4 + 96 + 4 + 31.5 + 24)
						System.out.println("4");
				}
			}
		};
	}
	
	private TPanel createBottom() {
		return new TPanel();
	}

}
