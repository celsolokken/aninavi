package in.celso.navi;

import in.celso.navi.anime.Anime;
import in.celso.navi.gui.NaviItem;
import in.celso.navi.manga.Manga;
import in.celso.navi.visualnovel.VisualNovel;

import java.util.List;

public class NaviItemManager extends Thread {
	
	public static class NaviItemFilter {
		public String nameSearch, authorSearch;
		public boolean series, anime, manga, vn;
		public int official, nsfw, oneshot, complete; //0 = allow official, 1 = only official, 2 = no official
		public int minYlevel, maxYlevel = 5;
	}
	
	private NaviItemFilter filter;
	private List<NaviItem> result;
	
	public NaviItemManager(NaviItemFilter filter, List<NaviItem> result) {
		this.filter = filter;
		this.result = result;
	}

	public void run() {
		List<Series> series = Fetcher.fetchSeries();
		for (Series s : series) {
			//System.out.println("Fetching " + s.name);
			/*try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
			if (filter.series) {
				if (validateSeries(filter, s)) {
					synchronized (result) {
						result.add(new NaviItem(s));
						result.notify();
					}
				}
			}
			else {
				if (filter.anime) {
					List<Anime> animes = s.animeList;
					for (Anime a : animes)
						if (validateEntry(filter, a)) {
							synchronized (result) {
								result.add(new NaviItem(a));
								result.notify();
							}
							//System.out.println("notifying from " +  Thread.currentThread().getName());
						}
				}
				if (filter.manga) {
					List<Manga> mangas = s.mangaList;
					for (Manga m : mangas)
						if (validateEntry(filter, m)) {
							synchronized (result) {
								result.add(new NaviItem(m));
								result.notify();
							}
							//System.out.println("notifying from " +  Thread.currentThread().getName());
						}
				}
				if (filter.vn) {
					//...
				}
			}
		}
		
		//System.out.println("final notifying from " + Thread.currentThread().getName());
		synchronized (result) {
			result.notify();
		}
		
		/*				
				if (items.get(items.size() - 1) == null)
					break;
				else
					System.out.println(items.get(items.size() - 1));*/
	}
	
	public List<NaviItem> getList() {

		List<Series> series = Fetcher.fetchSeries();
		for (Series s : series) {
			if (filter.series) {
				if (validateSeries(filter, s)) {
					result.add(new NaviItem(s));
				}
			}
			else {
				if (filter.anime) {
					List<Anime> animes = s.animeList;
					for (Anime a : animes)
						if (validateEntry(filter, a)) {
							result.add(new NaviItem(a));
						}
				}
				if (filter.manga) {
					List<Manga> mangas = s.mangaList;
					for (Manga m : mangas)
						if (validateEntry(filter, m)) {
							result.add(new NaviItem(m));
						}
				}
				if (filter.vn) {
					List<VisualNovel> vns = s.vnList;
					for (VisualNovel v : vns)
						if (validateEntry(filter, v)) {
							result.add(new NaviItem(v));
						}
				}
			}
		}
		
		return result;
	}
	
	private static boolean validateSeries(NaviItemFilter filter, Series series) {
		return filter.nameSearch == null || series.name.toLowerCase().contains(filter.nameSearch.toLowerCase());
	}
	
	private static boolean validateEntry(NaviItemFilter filter, Entry entry) {
		if (filter.nameSearch != null && !entry.name.toLowerCase().contains(filter.nameSearch.toLowerCase()))
			return false;
		if (filter.authorSearch != null && !entry.author.toLowerCase().contains(filter.authorSearch.toLowerCase()))
			return false;
		if ((filter.official == 1 && !entry.isOfficial) || (filter.official == 2 && entry.isOfficial))
			return false;
		if ((filter.nsfw == 1 && !entry.isNSFW) || (filter.nsfw == 2 && entry.isNSFW))
			return false;
		if ((filter.oneshot == 1 && !entry.isOneShot) || (filter.oneshot == 2 && entry.isOneShot))
			return false;
		if ((filter.complete == 1 && !entry.isComplete) || (filter.complete == 2 && entry.isComplete))
			return false;
		if (entry.yLevel < filter.minYlevel || entry.yLevel > filter.maxYlevel)
			return false;
		
		return true;
	}
	
}
