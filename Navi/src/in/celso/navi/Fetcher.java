package in.celso.navi;

import in.celso.navi.anime.Anime;
import in.celso.navi.manga.Manga;
import in.celso.navi.visualnovel.VisualNovel;

import java.util.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.Field;

import static in.celso.navi.NaviConstants.*;

import javax.imageio.ImageIO;

public class Fetcher {
	
	private static final Random rand = new Random();
	
	public static String naviHome = "/media/Data/navi";
	
	public static String[] fileOpeners = new String[4];
	
	private static List<Series> allSeries;
	
	private static Entry[] recents;
	
	public static synchronized Entry[] getRecentEntries() {
		if (recents != null)
			return recents;
		else {
			recents = new Entry[10];
			
			try (BufferedReader in = new BufferedReader(new FileReader(naviHome + "/recent.txt"))) {
				String line;
				int i = 0;
				while ((line = in.readLine()) != null) {
					String[] str = line.split("\\/");
					String type = str[0];
					Series series = getSeriesByName(str[1]);
					Entry entry = type.equals("a") ? getAnimeByName(series, str[2]) : 
									type.equals("m") ? getMangaByName(series, str[2]) : getVNByName(series, str[2]);
					recents[i++] = entry;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return recents;
		}
	}
	
	public static synchronized void addRecent(Entry entry) {
		for (int i = recents.length - 1; i > 0; i--)
			recents[i] = recents[i-1];
		recents[0] = entry;
	}
	
	public static synchronized void saveRecents() {
		try (PrintWriter out = new PrintWriter(naviHome + "/recent.txt")) {
			for (int i = 0; i < recents.length; i++) {
				Entry next = recents[i];
				if (next == null)
					break;
				String type = next instanceof Anime ? "a" : next instanceof Manga ? "m" : "v";
				out.println(type + "/" + next.series.name + "/" + next.name);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void createSeries(String name, String fileName) {
		File file = new File(naviHome + "/series/" + fileName + ".txt");
		
		try {
			file.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try (PrintWriter out = new PrintWriter(file)) {
			out.println("name=" + name);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void createAnime(Anime anime) {
		File folder = new File(naviHome + "/data/anime/" + anime.shortName);
		folder.mkdir();
		File info = new File(folder.getParent(), anime.shortName + ".txt");
		
		try (PrintWriter out = new PrintWriter(info)) {
			printEntryInfo(anime, out);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		File parentFile = new File(naviHome + "/series/" + anime.series.shortName + ".txt");
		try (PrintWriter out = new PrintWriter(new FileOutputStream(parentFile, true))) {
			out.println("a/" + anime.shortName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void createManga(Manga manga) {
		File folder = new File(naviHome + "/data/manga/" + manga.shortName);
		folder.mkdir();
		
		File info = new File(folder.getParent(), manga.shortName + ".txt");
		try (PrintWriter out = new PrintWriter(info)) {
			printEntryInfo(manga, out);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		File parentFile = new File(naviHome + "/series/" + manga.series.shortName + ".txt");
		try (PrintWriter out = new PrintWriter(new FileOutputStream(parentFile, true))) {
			out.println("m/" + manga.shortName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void createVN(VisualNovel vn) {
		File folder = new File(naviHome + "/data/visualnovel/" + vn.shortName);
		folder.mkdir();
		
		File info = new File(folder.getParent(), vn.shortName + ".txt");
		try (PrintWriter out = new PrintWriter(info)) {
			printEntryInfo(vn, out);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		File parentFile = new File(naviHome + "/series/" + vn.series.shortName + ".txt");
		try (PrintWriter out = new PrintWriter(new FileOutputStream(parentFile, true))) {
			out.println("b/" + vn.shortName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void openVisualNovel(VisualNovel vn) {
		try {
			Runtime.getRuntime().exec(fileOpeners[VN - 1] + " " + naviHome + "/Data/visualnovel/" + vn.shortName +
										"/" + vn.executablePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void printEntryInfo(Entry entry, PrintWriter out) {
		out.println("name=" + entry.name);
		out.println("author=" + entry.author);
		out.println("chronoPosition=" + entry.chronoPosition);
		out.println("prequel=" + entry.prequel);
		out.println("sequel=" + entry.sequel);
		out.println("isOneShot=" + entry.isOneShot);
		out.println("isOfficial=" + entry.isOfficial);
		out.println("isComplete=" + entry.isComplete);
		out.println("isNSFW=" + entry.isNSFW);
		out.println("yLevel=" + entry.yLevel);
		if (entry instanceof VisualNovel)
			out.println("executablePath=" + ((VisualNovel) entry).executablePath);
	}
	
	public static void moveEntry(Entry entry, Series newSeries) {//TODO
		Series currentSeries = entry.series;
		
		if (entry instanceof Anime) {
			currentSeries.animeList.remove(entry);
			newSeries.animeList.add((Anime) entry);
		}
		else if (entry instanceof Manga) {
			currentSeries.mangaList.remove(entry);
			newSeries.mangaList.add((Manga) entry);
		}
		else {
			currentSeries.vnList.remove(entry);
			newSeries.vnList.add((VisualNovel) entry);
		}
	}
	
	public static void deleteEntry(Entry entry) {//TODO
		String s = entry instanceof Anime ? "anime" : entry instanceof Manga ? "manga" : "visualnovel";
		File file = new File(naviHome + "/data/" + s + "/" + entry.shortName);
		
		try {
			Utils.copyDirectory(file, new File(naviHome + "/recyclebin/" + s + "/" + entry.shortName));
			Utils.delete(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (entry instanceof Anime) {
			entry.series.animeList.remove(entry);
		}
		else if (entry instanceof Manga) {
			entry.series.mangaList.remove(entry);
		}
		else {
			entry.series.vnList.remove(entry);
		}
	}
	
	public static synchronized List<Series> fetchSeries() {
		if (allSeries != null)
			return allSeries;
		
		List<Series> series = new ArrayList<>();
		File[] children = new File(naviHome + "/series").listFiles();
		for (File f : children) {
			if (!f.getName().endsWith(".txt"))
				continue;
			
			Series next = fetchSeriesFile(f);
			next.shortName = f.getName().substring(0, f.getName().length()-4);
			System.out.println(next.shortName);
			series.add(next);
		}
		return series;
	}
	
	private static Series fetchSeriesFile(File file) {
		Series next = new Series();
		next.animeList = new ArrayList<>();
		next.mangaList = new ArrayList<>();
		next.vnList = new ArrayList<>();
		
		try (BufferedReader in = new BufferedReader(new FileReader(file))) {
			next.name = in.readLine().split("=")[1];
			String line;
			while ((line = in.readLine()) != null) {
				String[] entryLine = line.split("/");
				int type = entryLine[0].equals("a") ? ANIME : entryLine[0].equals("m") ? MANGA : VN;
				
				Entry entry = null;
				
				if (type == ANIME) {
					entry = fetchEntry(entryLine[1], ANIME);
					next.animeList.add((Anime) entry);
				} 
				else if (type == MANGA) {
					entry = fetchEntry(entryLine[1], MANGA);
					next.mangaList.add((Manga) entry);
				}
				else {
					entry = fetchEntry(entryLine[1], VN);
					next.vnList.add((VisualNovel) entry);
				}
				
				entry.series = next;
				entry.shortName = entryLine[1];
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return next;
	}
	
	private static Entry fetchEntry(String entryName, int type) {
		String folder = null;
		Entry entry = null;
		
		if (type == ANIME) {
			folder = "anime";
			entry = new Anime();
		}
		else if (type == MANGA) {
			folder = "manga";
			entry = new Manga();
		}
		else {
			folder = "visualnovel";
			entry = new VisualNovel();
		}
		
		parseFields(entry, new File(naviHome + "/data/" + folder + "/" + entryName + ".txt"));
		return entry;
	}
	
	public static synchronized BufferedImage getCover(Series series) {
		File imgFile = new File(naviHome + "/series/" + series.shortName + ".jpg");
		if (!imgFile.exists()) {
			//System.out.println("Series: " + series.name + " is missing a cover image.");
			List<Anime> as = series.animeList;
			List<Manga> ms = series.mangaList;
			if (rand.nextInt(2) == 0) {
				if (!as.isEmpty())
					return getCover(as.get(rand.nextInt(as.size())), false);
				if (!ms.isEmpty())
					return getCover(ms.get(rand.nextInt(ms.size())), false);
			}
			else {
				if (!ms.isEmpty())
					return getCover(ms.get(rand.nextInt(ms.size())), false);
				if (!as.isEmpty())
					return getCover(as.get(rand.nextInt(as.size())), false);
			}
			return null;
		}
		
		try {
			return ImageIO.read(imgFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static synchronized BufferedImage getCover(Entry entry) {
		return getCover(entry, true);
	}
	
	public static synchronized BufferedImage getCover(Entry entry, boolean allowGetFromParent) {
		String s = entry instanceof Anime ? "anime" : entry instanceof Manga ? "manga" : "visualnovel";
		File imgFile = new File(naviHome + "/data/" + s + "/" + entry.shortName + ".jpg");
		
		if (!imgFile.exists()) {
			System.err.println("Entry: " + entry.name + " is missing a cover image.");
			if (allowGetFromParent) {
				return (BufferedImage) ImageCache.getImage(entry.series);
			}
			else
				return null;
		}
		
		try {
			return ImageIO.read(imgFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static synchronized Series getSeriesByName(String name) {
		List<Series> all = fetchSeries();
		for (Series series : all)
			if (series.name.equals(name))
				return series;
		
		throw new FetchException("Couldn't find series of name: " + name);
	}
	
	public static synchronized Anime getAnimeByName(Series series, String name) {
		List<Anime> animes = series.animeList;
		for (Anime a : animes)
			if (a.name.equals(name))
				return a;
		return null;
	}
	
	public static synchronized Manga getMangaByName(Series series, String name) {
		List<Manga> mangas = series.mangaList;
		for (Manga m : mangas)
			if (m.name.equals(name))
				return m;
		return null;
	}
	
	public static synchronized VisualNovel getVNByName(Series series, String name) {
		List<VisualNovel> animes = series.vnList;
		for (VisualNovel v : animes)
			if (v.name.equals(name))
				return v;
		return null;
	}
	
	public static synchronized void parseFields(Object obj, File info) {
		try (BufferedReader in = new BufferedReader(new FileReader(info))) {
			String line;
			while ((line = in.readLine()) != null) {
				String[] sides = line.split("=");
				Field field = obj.getClass().getField(sides[0]);
				Class<?> type = field.getType();
				
				if (sides.length == 1)
					continue;
				
				if (type == String.class)
					field.set(obj, sides[1]);
				else if (type.getName().equals("int"))
					field.set(obj, Integer.parseInt(sides[1]));
				else if (type == Double.class)
					field.set(obj, Double.parseDouble(sides[1]));
				else if (type.getName().equals("boolean"))
					field.set(obj, Boolean.parseBoolean(sides[1]));
				else if (type == Series.class)
					field.set(obj, getSeriesByName(sides[1]));
				else
					throw new FetchException("Unhandled type: " + type.getName() + " at line \n" + line + "\nat file: " +
												info + " for object: " + obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static class FetchException extends RuntimeException {
		public FetchException(String s) {
			super(s);
		}
		
		public FetchException() {
			super();
		}
	}
	
}
