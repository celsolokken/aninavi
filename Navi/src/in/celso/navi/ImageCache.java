package in.celso.navi;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public class ImageCache {
	
	private static Map<Object, Image> images = new HashMap<>();
	
	public static synchronized Image getImage(Entry entry) {
		if (images.containsKey(entry))
			return images.get(entry);
		
		Image img = Fetcher.getCover(entry);
		images.put(entry, img);
		return img;
	}
	
	public static synchronized Image getImage(Series series) {
		if (images.containsKey(series))
			return images.get(series);
		
		Image img = Fetcher.getCover(series);
		images.put(series, img);
		return img;
	}
	
	public static synchronized Image getImage(String resource) {
		if (images.containsKey(resource))
			return images.get(resource);
		
		Image img = null;
		
		try {
			img = Utils.createImage(resource);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		images.put(resource, img);
		return img;
	}

}
