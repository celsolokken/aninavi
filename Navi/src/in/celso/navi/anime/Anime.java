package in.celso.navi.anime;

import java.util.List;

import in.celso.navi.Entry;

public class Anime extends Entry {
	
	public Anime(String name, String author, int chronoPosition,
			String prequel, String sequel, boolean isOneShot,
			boolean isOfficial, boolean isComplete,
			boolean isNSFW, int yLevel) {
		super(name, author, chronoPosition, prequel, sequel, isOneShot, isOfficial, isComplete, isNSFW, yLevel);
	}
	
	public Anime() {
		
	}
	
	public List<AnimeEpisode> episodes;
	
}
