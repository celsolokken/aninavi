package in.celso.navi.gui;

import in.celso.navi.Entry;
import in.celso.navi.Fetcher;
import in.celso.navi.NaviConstants;
import in.celso.navi.NaviItemManager;
import in.celso.navi.Series;
import in.celso.navi.Settings;
import in.celso.navi.Utils;
import in.celso.navi.NaviItemManager.NaviItemFilter;
import in.celso.navi.anime.Anime;
import in.celso.navi.manga.Manga;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import javax.swing.*;
import javax.swing.plaf.ScrollBarUI;
import javax.swing.plaf.basic.BasicScrollBarUI;

public class Navi extends JFrame {
	
	private TrayIcon trayIcon;
	
	private JSplitPane mainSplit;
	
	private Stack<Container> previous = new Stack<>();
	private java.util.List<Container> nexts = new ArrayList<>();
	
	private HomePanel homePanel;
	
	private CButton backButton, homeButton, nextButton;
	
	public Navi() {
		//setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(900, 700);
		
		NaviItemManager.NaviItemFilter filter = new NaviItemManager.NaviItemFilter();
		filter.anime = true;
		filter.manga = true;
		filter.oneshot = 2;
		//filter.series = true;
		
		homePanel = new HomePanel(this);
		
		JPanel rightPanel = homePanel;
		mainSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, createTopPanel(), rightPanel);
		mainSplit.setDividerSize(0);
		mainSplit.setEnabled(false);
		
		setContentPane(mainSplit);		
		setLocationRelativeTo(null);
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.out.println("Saindo");
				Fetcher.saveRecents();
			}
		});
		
		//trayIcon = createTrayIcon();
	}
	
	public void setCurrentPanel(Container cont, boolean saveCurrent) {
		setCurrentPanel(cont, saveCurrent, false);
	}
	
	public void setCurrentPanel(Container cont, boolean saveCurrent, boolean goingBack) {
		if (cont == (Container) mainSplit.getRightComponent())
			return;
		if (saveCurrent)
			previous.push((Container) mainSplit.getRightComponent());
		if (!goingBack && !nexts.isEmpty() && cont != nexts.get(nexts.size() - 1)) {
			nexts.clear();
			nextButton.setEnabled(false);
		}
		
		mainSplit.setRightComponent(cont);
		backButton.setEnabled(!previous.isEmpty());
		mainSplit.setDividerLocation(43);
	}
	
	private void goBack() {
		if (nexts.isEmpty() || (Container) mainSplit.getRightComponent() != nexts.get(0)) {
			nexts.add(0, (Container) mainSplit.getRightComponent());
			nextButton.setEnabled(true);
		}
		
		Container previous = this.previous.pop();
		assert previous != null;
		setCurrentPanel(previous, false, true);
		if (this.previous.isEmpty())
			backButton.setEnabled(false);
	}
	
	private void goForward() {
		previous.push((Container) mainSplit.getRightComponent());
		Container next = nexts.remove(0);
		assert next != null;
		setCurrentPanel(next, false, true);
		if (nexts.isEmpty())
			nextButton.setEnabled(false);
	}
	
	protected void popLast() {
		previous.pop();
		if (previous.isEmpty())
			backButton.setEnabled(false);
	}
	
	protected void naviItemClicked(NaviItem item) {
		if (item.type == NaviConstants.SERIES) {
			JScrollPane rightPane = new JScrollPane(new SeriesPanel(this, item.series));
			rightPane.getVerticalScrollBar().setUnitIncrement(5);
			rightPane.getVerticalScrollBar().setUI(Navi.CUSTOM_SCROLLBAR);
			setCurrentPanel(rightPane, true);
		}
		else {
			JPanel rightPanel = new EntryPanel(item.entry);
			setCurrentPanel(rightPanel, true);
			setCursor(Cursor.getDefaultCursor());
			
			Fetcher.addRecent(item.entry);
			homePanel.addRecent(item);
		}
	}
	
	private JPanel createTopPanel() {
		JPanel jp = new JPanel2();
		jp.setLayout(new FlowLayout());
		
		Image backImage = null, nextImage = null, homeImage = null;
		try {
			backImage = Utils.createImage("~back0.png");
			nextImage = Utils.createImage("~next0.png");
			homeImage = Utils.createImage("~home0.png");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		backButton = new CButton(backImage);
		nextButton = new CButton(nextImage);
		homeButton = new CButton(homeImage);
		
		jp.add(homeButton);
		jp.add(backButton);
		jp.add(nextButton);
		
		Dimension dim = new Dimension(32, 32);
		backButton.setPreferredSize(dim);
		nextButton.setPreferredSize(dim);
		homeButton.setPreferredSize(dim);
		
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				goBack();
			}
		});
		
		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				goForward();
			}
		});
		
		homeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (mainSplit.getTopComponent() != homePanel)
					setCurrentPanel(homePanel, true);
			}
		});
		
		backButton.setEnabled(false);
		nextButton.setEnabled(false);
		
		return jp;
	}
	
	private TrayIcon createTrayIcon() {
		TrayIcon trayIcon = null;
		
		try {
			trayIcon = new TrayIcon(Utils.createImage(Settings.trayIcon), "Navigator");
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		trayIcon.setImageAutoSize(true);
		
		ActionListener toggleFrame = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (isVisible()) {
					setVisible(false);					
				} else {
					setVisible(true);
					toFront();
				}
			}
		};
		
		trayIcon.addActionListener(toggleFrame);
		
		PopupMenu popMenu = new PopupMenu();

		MenuItem showHide = new MenuItem("Show/Hide");
		MenuItem exit = new MenuItem("Exit");
		
		showHide.addActionListener(toggleFrame);
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				System.exit(0);
			}
		});
		
		popMenu.add(showHide);
		popMenu.add(exit);
		
		trayIcon.setPopupMenu(popMenu);
		
		try {
			SystemTray.getSystemTray().add(trayIcon);
		} catch (AWTException e1) {
			e1.printStackTrace();
		}
		
		return trayIcon;
	}
	
	private static class JPanel2 extends JPanel {
		public void paintComponent(Graphics g) {
			int wid = getWidth();
			int hei = getHeight();
			int bgSize = 64;
			for (int x = 0; x <= wid / bgSize; x++)
				for (int y = 0; y <= hei / bgSize; y++) {
					//g.drawImage(bgImage, x * bgSize, y * bgSize, bgSize, bgSize, null);
					int g2 = 244 - ((x%2)+10+y)*4, b2 = 182 - ((x%2)+10+y)*4;
					g.setColor(new Color(255, g2 > 0 ? g2 : 0, b2 > 0 ? b2 : 0, 255));
					g.fillRect(x * bgSize, y * bgSize, bgSize, bgSize);
				}
			g.setColor(new Color(255, 255, 255, 60));
			g.fillRect(0, 0, wid, hei/2);
			
			g.fillRect(0, hei - 1, wid, 1);
		}
	}
	
	public static final BasicScrollBarUI CUSTOM_SCROLLBAR = new MyBar();
	
	private static class MyBar extends BasicScrollBarUI {
		
		protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
			g.setColor(new Color(255, 169, 60));
			g.fillRect(thumbBounds.x + 2, thumbBounds.y + 2, thumbBounds.width - 4, thumbBounds.height - 4);
		}
		
		protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
			g.setColor(new Color(120, 110, 100));
			g.fillRect(0, 0, trackBounds.width, trackBounds.height);
		}
		
		public Dimension getPreferredSize(JComponent c) {
			Dimension d = super.getPreferredSize(c);
			d.width = 10;
			d.height = 10;
			return d;
		}
		
        protected JButton createDecreaseButton(int orientation) {
            return createZeroButton();
        }

        protected JButton createIncreaseButton(int orientation) {
            return createZeroButton();
        }
		
        private JButton createZeroButton() {
            JButton jbutton = new JButton();
            jbutton.setPreferredSize(new Dimension(0, 0));
            jbutton.setMinimumSize(new Dimension(0, 0));
            jbutton.setMaximumSize(new Dimension(0, 0));
            return jbutton;
        }

	}
	
}
