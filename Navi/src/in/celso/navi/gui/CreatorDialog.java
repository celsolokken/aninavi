package in.celso.navi.gui;

import in.celso.navi.Entry;
import in.celso.navi.Fetcher;
import in.celso.navi.ImageCache;
import in.celso.navi.NaviConstants;
import in.celso.navi.Series;
import in.celso.navi.anime.Anime;
import in.celso.navi.manga.Manga;
import in.celso.navi.visualnovel.VisualNovel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdesktop.swingx.border.DropShadowBorder;

public class CreatorDialog extends OPanel {
	
	private Navi navi;
	
	private int type;
	
	public CreatorDialog(Navi navi, int type, Series series) {
		this.navi = navi;
		this.type = type;
		createEntry(series);
	}
	
	private void createEntry(final Series series) {
		JPanel jp = new JPanel() {
			protected void paintComponent(Graphics g) {
				((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
				
				paintBorder(g);
				g.clipRect(10, 10, getWidth() - 20, getHeight() - 20);
				g.translate(10, 10);
				
				if (type == NaviConstants.ANIME)
					GUtils.paintChess(g, getWidth() - 10, getHeight() - 10, 32, 110, 110, 180);
				else if (type == NaviConstants.MANGA)
					GUtils.paintChess(g, getWidth() - 10, getHeight() - 10, 32, 110, 110, 110);
				else
					GUtils.paintChess(g, getWidth() - 10, getHeight() - 10, 32, 100, 150, 100);
				
			}
		};
		
		DropShadowBorder dsb = new DropShadowBorder(Color.BLACK, 10);
		dsb.setShadowOpacity(0.1f);
		dsb.setShowBottomShadow(true);
		dsb.setShowTopShadow(true);
		dsb.setShowLeftShadow(true);
		dsb.setShowRightShadow(true);
		jp.setBorder(dsb);
		
		JButton goButton = new JButton("Create");
		
		final JTextField nameField = new JTextField();
		final JTextField authorField = new JTextField();
		final JCheckBox oneshotBox = new JCheckBox("Is one shot?");
		final JCheckBox doujinBox = new JCheckBox("Is doujinshi?");
		final JCheckBox completeBox = new JCheckBox("Is complete?");
		final JCheckBox nsfwBox = new JCheckBox("Is NSFW?");
		final RankPicker yLevel = new RankPicker(3, Color.BLACK, Color.YELLOW);
		
		ActionListener go = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Entry entry = type == NaviConstants.ANIME ? new Anime(nameField.getText(),
						authorField.getText(), 1, null, null,
						oneshotBox.isSelected(), !doujinBox.isSelected(),
						completeBox.isSelected(), nsfwBox.isSelected(),
						3/*Integer.parseInt(ylevel.getText())*/)
					: type == NaviConstants.MANGA ? new Manga(nameField.getText(),
							authorField.getText(), 1, null, null,
							oneshotBox.isSelected(), !doujinBox.isSelected(),
							completeBox.isSelected(), nsfwBox.isSelected(),
							3/*Integer.parseInt(ylevel.getText())*/)
					: new VisualNovel(nameField.getText(),
							authorField.getText(), 1, null, null,
							oneshotBox.isSelected(), !doujinBox.isSelected(),
							completeBox.isSelected(), nsfwBox.isSelected(),
							3/*Integer.parseInt(ylevel.getText())*/);
				
				entry.series = series;
				entry.shortName = entry.name.toLowerCase().replaceAll(" ", "");
				
				if (type == NaviConstants.ANIME) {
					Fetcher.createAnime((Anime) entry);
					series.animeList.add((Anime) entry);
				}
				else if (type == NaviConstants.MANGA) {
					Fetcher.createManga((Manga) entry);
					series.mangaList.add((Manga) entry);
				}
				else {
					Fetcher.createVN((VisualNovel) entry);
					series.vnList.add((VisualNovel) entry);
				}
				
				navi.setCurrentPanel(new SeriesPanel(navi, series), false);
				navi.popLast();
			}
		};
		
		goButton.addActionListener(go);
		nameField.addActionListener(go);
		
		oneshotBox.setForeground(Color.WHITE);
		doujinBox.setForeground(Color.WHITE);
		completeBox.setForeground(Color.WHITE);
		nsfwBox.setForeground(Color.WHITE);
		
		jp.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		
		c.gridwidth = 3;
		jp.add(new JLabel(new ImageIcon(ImageCache.getImage(Fetcher.naviHome + "/res/cover.jpg"))), c);
		
		c.gridy++;
		c.insets = new Insets(5, 0, 15, 0);
		JLabel seriesName = new JLabel("Series: " + series.name, JLabel.CENTER);
		seriesName.setForeground(Color.WHITE);
		jp.add(seriesName, c);
		
		c.insets = new Insets(0, 0, 0, 0);
		c.gridwidth = 1;
		c.gridy++;
		JLabel name = new JLabel("Name: ");
		name.setForeground(Color.WHITE);
		jp.add(name, c);
		
		c.gridx += c.gridwidth;
		c.gridwidth = 2;
		c.ipadx = 100;
		jp.add(nameField, c);
		
		c.gridx = 0;
		c.gridwidth = 1;
		c.ipadx = 0;
		c.gridy++;
		JLabel author = new JLabel("Author: ");
		author.setForeground(Color.WHITE);
		jp.add(author, c);
		
		c.gridx += c.gridwidth;
		c.gridwidth = 2;
		c.ipadx = 100;
		jp.add(authorField, c);
		
		c.ipadx = 0;
		c.gridx = 0;
		c.gridy++;
		jp.add(oneshotBox, c);
		
		c.gridy++;
		jp.add(doujinBox, c);
		
		c.gridy++;
		jp.add(completeBox, c);
		
		c.gridy++;
		jp.add(nsfwBox, c);
		
		c.gridy++;
		c.insets = new Insets(5, 50, 0, 50);
		jp.add(yLevel, c);
		
		c.gridx = 0;
		c.gridy++;
		c.gridwidth = 3;
		c.ipadx = 0;
		jp.add(goButton, c);
		
		setLayout(new GridBagLayout());
		add(jp);
		jp.setPreferredSize(new Dimension(jp.getPreferredSize().width + 20 + 20, jp.getPreferredSize().height + 20 + 20));
	}
	
}
