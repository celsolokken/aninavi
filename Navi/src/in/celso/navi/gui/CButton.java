package in.celso.navi.gui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JComponent;

public class CButton extends JComponent implements MouseListener {
	private Image currentImage;
	private Image image, imageHover, imagePressed, imageDisabled;
	private String text;
	private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();
	
	private boolean canDisable;
	
	private Color shadowColor = new Color(0, 0, 0, 130);;
	
	private int textWid, textHei;
	
	private boolean mouseIn, mouseDown;
	
	public CButton(Image image) {
		this(image, image, image, null);
	}
	
	public CButton(Image image, Image imageHover, Image imagePressed) {
		this(image, imageHover, imagePressed, null);
	}

	public CButton(Image image, Image imageHover, Image imagePressed, Image imageDisabled) {
		this.image = image;
		this.imageHover = imageHover;
		this.imagePressed = imagePressed;
		this.imageDisabled = imageDisabled;
		addMouseListener(this);
		
		canDisable = imageDisabled != null;
	}
	
	public void setText(String text) {
		setText(text, Color.BLACK);
	}
	
	public void setText(String text, Color textColor) {
		this.text = text;
		setForeground(textColor);
	}

	public void paintComponent(Graphics g) {
		//super.paintComponent(g);
		
		Image toDraw;
		
		if (isEnabled() || !canDisable) {
			if (mouseIn) {
				if (mouseDown)
					toDraw = imagePressed;
				else
					toDraw = imageHover;
			}
			else
				toDraw = image;
		}
		else {
			toDraw = imageDisabled;
		}
		
		if (toDraw != currentImage)
			currentImage = toDraw;
		//else
			//return;
		
		AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, isEnabled() ? 1 : 0.1f);
		((Graphics2D) g).setComposite(ac);
		g.drawImage(toDraw, 0, 0, getBounds().width, getBounds().height, null);
		
		if (text != null) {
			
			g.setColor(getForeground());
			Rectangle2D bounds = g.getFontMetrics().getStringBounds(text, g);
			if (mouseIn && mouseDown) {
				g.drawString(text, getBounds().width/2 - (int) bounds.getWidth()/2 + 2, getBounds().height/2 + (int) bounds.getHeight()/4 + 2);
				//g.setColor(shadowColor);
				//g.drawString(text, getBounds().width/2 - (int) bounds.getWidth()/2 + 2 + 1, getBounds().height/2 + (int) bounds.getHeight()/4 + 2 + 1);
			}
			else {
				g.drawString(text, getBounds().width/2 - (int) bounds.getWidth()/2, getBounds().height/2 + (int) bounds.getHeight()/4);
				//g.setColor(shadowColor);
				//g.drawString(text, getBounds().width/2 - (int) bounds.getWidth()/2 + 1, getBounds().height/2 + (int) bounds.getHeight()/4 + 1);
			}
		}
	}

	public void addActionListener(ActionListener al) {
		listeners.add(al);
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		mouseDown = true;
		repaint();
	}

	public void mouseReleased(MouseEvent e) {
		mouseDown = false;
		repaint();
		int x = e.getX();
		int y = e.getY();
		if (x > 0 && x <= getBounds().width && y > 0 && y <= getBounds().height)
			notifyListeners(e);
	}

	public void mouseEntered(MouseEvent e) {
		mouseIn = true;
		repaint();
	}

	public void mouseExited(MouseEvent e) {
		mouseIn = false;
		repaint();
	}
	
	private void notifyListeners(MouseEvent e) {
		if (!isEnabled())
			return;
		
		synchronized(listeners) {
			for (int i = 0; i < listeners.size(); i++)
				listeners.get(i).actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, ""));
		}
	}

}
