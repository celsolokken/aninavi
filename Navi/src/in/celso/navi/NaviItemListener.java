package in.celso.navi;

import in.celso.navi.gui.NaviItem;

public interface NaviItemListener {
	
	public void onClick(NaviItem item);

}
