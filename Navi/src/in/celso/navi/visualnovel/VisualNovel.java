package in.celso.navi.visualnovel;

import in.celso.navi.Entry;

public class VisualNovel extends Entry {

	public VisualNovel(String name, String author, int chronoPosition,
			String prequel, String sequel, boolean isOneShot,
			boolean isOfficial, boolean isComplete,
			boolean isNSFW, int yLevel) {
		super(name, author, chronoPosition, prequel, sequel, isOneShot, isOfficial, isComplete, isNSFW, yLevel);
	}

	public VisualNovel() {
		
	}
	
	public String executablePath;

}
