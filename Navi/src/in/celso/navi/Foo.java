package in.celso.navi;

import in.celso.navi.anime.Anime;
import in.celso.navi.gui.CreatorDialog;
import in.celso.navi.gui.NaviItem;
import in.celso.navi.manga.Manga;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class Foo {
	
	public static void main(String[] args) {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		/*Series s = new Series();
		s.animeList = new ArrayList<>();
		s.mangaList = new ArrayList<>();
		s.vnList = new ArrayList<>();
		s.animeList.add(new Anime());
		s.animeList.add(new Anime());
		s.animeList.add(new Anime());
		s.animeList.add(new Anime());
		s.mangaList.add(new Manga());
		s.mangaList.add(new Manga());
		s.name = "Mahou Shoujo Madoka★Magica";
		NaviItem ni = new NaviItem(s);
		
		Manga m = new Manga();
		m.name = "Akuma no Riddle";
		m.isNSFW = true;
		m.isOneShot = true;
		m.yLevel = 3;
		m.isComplete = false;
		NaviItem ni2 = new NaviItem(m);*/
		
		final JFrame f = new JFrame();
		f.setLayout(new FlowLayout());
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLocationRelativeTo(null);
		
		NaviItemManager.NaviItemFilter filter = new NaviItemManager.NaviItemFilter();
		filter.anime = true;
		filter.manga = true;
		
		final List<NaviItem> items = new ArrayList<NaviItem>();
		NaviItemManager nim = new NaviItemManager(filter, items);
		nim.start();
		int added = 0;
		while (true) {
			synchronized (items) {
				System.out.println("Waiting for items at Thread " + Thread.currentThread().getName());
				try {
					items.wait(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				System.out.println("Finished waiting " + items.size());
				
				if (items.size() == ++added) {
					final int next = added - 1;
					
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							f.add(items.get(next));
							f.pack();
							f.setLocationRelativeTo(null);
							if (!f.isVisible())
								f.setVisible(true);
							f.revalidate();
						}
					});
				}
				else
					break;
			}
		}
		
		System.out.println("Outside");
		
		//System.out.println(CreatorDialog.createEntry(f, null));
		
		System.out.println("lol");
	}

}
