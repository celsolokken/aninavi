package in.celso.navi;

public class Entry {
	
	public String name;
	
	public String author;
	
	public int chronoPosition;
	
	public String prequel, sequel;
	
	public Series series;
	
	public boolean isOneShot;
	
	public boolean isOfficial;
	
	public boolean isComplete;
	
	public boolean isNSFW;
	
	public int yLevel;
	
	public String shortName;
	
	public String info;
	
	public Entry(String name, String author, int chronoPosition,
			String prequel, String sequel, boolean isOneShot,
			boolean isOfficial, boolean isComplete,
			boolean isNSFW, int yLevel) {
		this.name = name;
		this.author = author;
		this.chronoPosition = chronoPosition;
		this.prequel = prequel;
		this.sequel = sequel;
		this.isOneShot = isOneShot;
		this.isOfficial = isOfficial;
		this.isComplete = isComplete;
		this.isNSFW = isNSFW;
		this.yLevel = yLevel;
	}
	
	public Entry() {
		
	}

	public void print() {
		System.out.println("name=" + name);
		System.out.println("author=" + author);
		System.out.println("chronoPosition=" + chronoPosition);
		System.out.println("prequel=" + prequel);
		System.out.println("sequel=" + sequel);
		System.out.println("series=" + series.name);
		System.out.println("isOneShot=" + isOneShot);
		System.out.println("isOfficial=" + isOfficial);
		System.out.println("isComplete=" + isComplete);
		System.out.println("isNSFW=" + isNSFW);
		System.out.println("yLevel=" + yLevel);
		System.out.println("shortName=" + shortName);
	}
	
}
